package IPL;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class BowlerData implements WritableComparable<BowlerData> {
    private IntWritable runs;
    private IntWritable delivery;

    public BowlerData() {
        runs = new IntWritable(0);
        delivery = new IntWritable(0);
    }

    public void write(DataOutput dataOutput) throws IOException {
        runs.write(dataOutput);
        delivery.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        runs.readFields(dataInput);
        delivery.readFields(dataInput);
    }

    public IntWritable getRuns() {
        return runs;
    }

    public void setRuns(IntWritable runs) {
        this.runs = runs;
    }

    public IntWritable getDelivery() {
        return delivery;
    }

    public void setDelivery(IntWritable delivery) {
        this.delivery = delivery;
    }
    
    @Override
    public String toString() {
        return (runs.toString()+","+delivery.toString());
    }

    public int compareTo(BowlerData o) {
        String thisRuns = this.runs.toString();
        String thatRuns = o.runs.toString();
        String thisDelivery = this.delivery.toString();
        String thatDelivery = o.delivery.toString();
        return thisRuns.compareTo(thatRuns) + thisDelivery.compareTo(thatDelivery);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BowlerData that = (BowlerData) o;
        return Objects.equals(delivery, that.delivery) &&
                Objects.equals(runs, that.runs);
    }
    @Override
    public int hashCode() {
        return Objects.hash(runs, delivery);
    }
}