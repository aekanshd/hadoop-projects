 package IPL;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import IPL.FinalData;

public class IPLFinalMapper extends Mapper<LongWritable, Text, FinalData, NullWritable> {
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		String valueString = value.toString();
		String[] lineData = valueString.split(",");

		FinalData finalData = new FinalData();
		finalData.setBatsman(new Text(lineData[0]));
		finalData.setBowler(new Text(lineData[1]));
		finalData.setRuns(new IntWritable(Integer.parseInt(lineData[2])));
		finalData.setDeliveries(new IntWritable(Integer.parseInt(lineData[3])));
		
		context.write(finalData, NullWritable.get());
	}
}
