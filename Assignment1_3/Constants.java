package IPL;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;

class Constants {
	public static final int col_info = 0; // What type of data was this?
	public static final int col_batsman = 4; // Who was the striker
	public static final int col_bowler = 6; // Who was the bowler?
	public static final int col_runs = 7; // How many runs?
	public static final int col_extras = 8; // How many extras?
	public static final int col_wicket = 9; // How did he get out?
	public static final int col_batsman_out = 10; // Who got out?
	public static final int col_stadium = 2; // Stadium name
	public static final IntWritable zero = new IntWritable(0);
	public static final IntWritable one = new IntWritable(1);
}
