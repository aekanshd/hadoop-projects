package IPL;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;

public class NaturalKeyGroupingComparator extends WritableComparator {

	protected NaturalKeyGroupingComparator() {
		super(IPL.FinalData.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable wc1, WritableComparable wc2) {
		FinalData k1 = (FinalData)wc1;
		FinalData k2 = (FinalData)wc2;
		
		// Extra code written for full sorting

        Text bm2 = k2.getBatsman();
        Text bo2 = k2.getBowler();
        Text bm1 = k1.getBatsman();
        Text bo1 = k1.getBowler();
        IntWritable w2 = k2.getRuns();
        IntWritable d2 = k2.getDeliveries();
        IntWritable w1 = k1.getRuns();
        IntWritable d1 = k1.getDeliveries();

        int wi1, wi2, di1, di2;
        wi1 = w1.get();
        wi2 = w2.get();
        di1 = d1.get();
        di2 = d2.get();


        if(wi1 != wi2) {
            return (wi1 > wi2 ? 1 : -1);
        }
        if(di1 != di2) {
            return (di1 > di2 ? 1 : -1);
        }
        if(bo1.compareTo(bo2) != 0) {
            return bo1.compareTo(bo2);
        }
        if(bm1.compareTo(bm2) != 0) {
            return bm1.compareTo(bm2);
        }
        
        return 0;
	}
}
