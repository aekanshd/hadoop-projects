#!/bin/sh
clear
export CLASSPATH="/usr/local/hadoop/share/hadoop/common/hadoop-common-2.7.2.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.7.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar"
export CLASSPATH=$CLASSPATH:"~/Desktop/hadoop-projects/Assignment1_3/"
rm -R IPL/ 
rm Assignment1_3.jar
$HADOOP_HOME/bin/hdfs dfs -rm /IPLTestDataTmp/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLTestDataTmp 
$HADOOP_HOME/bin/hdfs dfs -rm /IPLData_out_1_2/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLData_out_1_2/
javac -d . IPLDriver.java IPLMapper.java IPLFinalMapper.java IPLReducer.java Constants.java FinalData.java BowlerData.java BatBowl.java NaturalKeyPartitioner.java NaturalKeyGroupingComparator.java CompositeKeyComparator.java IPLFinalReducer.java
jar cfm Assignment1_3.jar Manifest.txt IPL/*.class
$HADOOP_HOME/bin/hadoop jar Assignment1_3.jar /IPLData /IPLData_out_1_2
$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out_1_2/part-r-00000