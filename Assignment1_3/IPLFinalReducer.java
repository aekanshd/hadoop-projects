package IPL;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IPLFinalReducer extends Reducer<FinalData, Text, FinalData, NullWritable> {

	public void reduce(FinalData t_key, NullWritable values, Context context) throws IOException, InterruptedException {		
            context.write(t_key, NullWritable.get());		
	}
}
