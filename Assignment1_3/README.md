# IPL Data

## Tasks

1. Find the number of wickets/deliveries a bowler has taken against a batsman. (Assignment1_2)
2. **Find the number of run/deliveries a batsman has taken against a bowler.** (Assignment1_3)
3. Find the number of run/stadium by a batsman. (Assignment1_4)

## Compile and run

1. Delete the `IPL` folder and any `Assignment1_*.jars`.
2. Soft Link this folder to home of `hduser`.
   ```shell
   ln -s path/to/Assignment1_2/ ~/
   ```
3. Export `$CLASSPATH`:

    ```shell
    export CLASSPATH="$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.2.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-common-3.2.0.jar:$HADOOP_HOME/share/hadoop/common/hadoop-common-3.2.0.jar:~/Assignment1_2/*:$HADOOP_HOME/lib/*"
    ```
4. Compile the code:
   ```shell
   javac -d . IPLDriver.java IPLMapper.java IPLFinalMapper.java IPLReducer.java Constants.java FinalData.java BowlerData.java BatBowl.java NaturalKeyPartitioner.java NaturalKeyGroupingComparator.java CompositeKeyComparator.java IPLFinalReducer.java
   ```
5. Make the JAR:
	```shell
	jar cfm Assignment1_2.jar Manifest.txt IPL/*.class
	```
6. Copy the dataset to Hadoop DFS:
	```shell
	mkdir ~/IPLData
	cp datasets/alldata.csv ~/IPLData
	$HADOOP_HOME/bin/hdfs dfs -put ~/IPLData /
	```
7. Run the JAR on the dataset:
	```shell
	$HADOOP_HOME/bin/hadoop jar Assignment1_2.jar /IPLData /IPLData_out
	```
8. View the entire file, export it, or grep it:
	```shell
	$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out/part-r-00000
	$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out/part-r-00000 >> final_output.csv
	$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out/part-r-00000 | grep "Kohli"
	$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out/part-r-00000 | grep "Kohli" | grep "Steyn"
	```