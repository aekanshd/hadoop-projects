package IPL;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class FinalData implements WritableComparable<FinalData> {
    private Text batsman;
    private Text bowler;
    private IntWritable runs;
    private IntWritable deliveries;

    public FinalData() {
        batsman = new Text();
        bowler = new Text();
        runs = new IntWritable(0);
        deliveries = new IntWritable(0);
    }

    public void write(DataOutput dataOutput) throws IOException {
        batsman.write(dataOutput);
        bowler.write(dataOutput);
        runs.write(dataOutput);
        deliveries.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        batsman.readFields(dataInput);
        bowler.readFields(dataInput);
        runs.readFields(dataInput);
        deliveries.readFields(dataInput);
    }

    public Text getBatsman() {
        return batsman;
    }

    public void setBatsman(Text batsman) {
        this.batsman = batsman;
    }

    public Text getBowler() {
        return bowler;
    }

    public void setBowler(Text bowler) {
        this.bowler = bowler;
    }

    public IntWritable getRuns() {
        return runs;
    }

    public void setRuns(IntWritable runs) {
        this.runs = runs;
    }

    public IntWritable getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(IntWritable deliveries) {
        this.deliveries = deliveries;
    }


    @Override
    public String toString() {
        return (batsman + "," + bowler + "," + runs.toString() + "," + deliveries.toString());
        // return (runs.toString() + "," + deliveries.toString() + "," + batsman + "," + bowler);
    }

    @Override
    public int compareTo(FinalData o) {
        System.out.println("Inside compareTo of FinalData.");
        
        Text bm2 = o.batsman;
        Text bo2 = o.bowler;
        Text bm1 = this.batsman;
        Text bo1 = this.bowler;
        IntWritable w2 = o.runs;
        IntWritable d2 = o.deliveries;
        IntWritable w1 = this.runs;
        IntWritable d1 = this.deliveries;

        int wi1, wi2, di1, di2;
        wi1 = w1.get();
        wi2 = w2.get();
        di1 = d1.get();
        di2 = d2.get();


        if(wi1 != wi2) {
            return -(wi1 - wi2);
        }
        if(di1 != di2) {
            return (di1 - di2);
        }
        if(bo1.compareTo(bo2) != 0) {
            return bo1.compareTo(bo2);
        }
        if(bm1.compareTo(bm2) != 0) {
            return bm1.compareTo(bm2);
        }
        
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinalData that = (FinalData) o;
        return Objects.equals(batsman, that.batsman) &&
                Objects.equals(bowler, that.bowler) &&
                Objects.equals(runs, that.runs) &&
                Objects.equals(deliveries, that.deliveries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.batsman, this.bowler, this.runs, this.deliveries);
    }
}