package IPLTest;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

public class IPLReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

	public void reduce(Text t_key, Iterator<IntWritable> values, OutputCollector<Text,IntWritable> output, Reporter reporter) throws IOException {
		System.out.println("Inside IPLReducer!");
		
		int runs_given = 0;
		
		while (values.hasNext()) {
			IntWritable runs = (IntWritable) values.next();
            runs_given += runs.get();
        }

		output.collect(t_key, new IntWritable(runs_given));
	}
}
