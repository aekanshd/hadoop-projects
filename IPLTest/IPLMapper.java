package IPLTest;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

public class IPLMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

	public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {

		System.out.println("Inside IPLMapper!");
		String valueString = value.toString();
		String[] BatsmanData = valueString.split(",");
		if(BatsmanData[0].equalsIgnoreCase("ball") && BatsmanData[4].equalsIgnoreCase("V Kohli"))
		{
			output.collect(new Text(BatsmanData[6]), new IntWritable(Integer.parseInt(BatsmanData[7])+Integer.parseInt(BatsmanData[8])));
		}
	}
}
