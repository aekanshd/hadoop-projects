package IPL;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.io.Text;
import java.io.IOException;

public class NaturalKeyPartitioner extends Partitioner<FinalData, Text> {

	@Override
	public int getPartition(FinalData key, Text val, int numPartitions) {
		if (numPartitions == 0) {return 0;}

		int hash = key.hashCode();
		int partition = hash % numPartitions;
		return partition;
	}

}
