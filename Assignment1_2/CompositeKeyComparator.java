package IPL;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.IOException;

public class CompositeKeyComparator extends WritableComparator {

	protected CompositeKeyComparator() {
		super(IPL.FinalData.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		// This compare is finally implemented
		FinalData k1 = (FinalData)w1;
		FinalData k2 = (FinalData)w2;
		
		int result = -1* k1.getWickets().compareTo(k2.getWickets());

		if(0 == result) {
			result = k1.getDeliveries().compareTo(k2.getDeliveries());
			if(0 == result) {
				result = k1.getBatsman().compareTo(k2.getBatsman());
				if(0 == result) {
					result = k1.getBowler().compareTo(k2.getBowler());
				}
			}
		}

		return result;
	}
}