package IPL;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class BatBowl implements WritableComparable<BatBowl> {
    private Text batsman;
    private Text bowler;

    public BatBowl() {
        batsman = new Text();
        bowler = new Text();
    }

    public void write(DataOutput dataOutput) throws IOException {
        batsman.write(dataOutput);
        bowler.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        batsman.readFields(dataInput);
        bowler.readFields(dataInput);
    }

    public Text getBatsman() {
        return batsman;
    }

    public void setBatsman(Text batsman) {
        this.batsman = batsman;
    }

    public Text getBowler() {
        return bowler;
    }

    public void setBowler(Text bowler) {
        this.bowler = bowler;
    }

    @Override
    public String toString() {
        return (batsman+","+bowler);
    }

    public int compareTo(BatBowl o) {
        String thisBatsman = this.batsman.toString();
        String thatBatsman = o.batsman.toString();
        String thisBowler = this.bowler.toString();
        String thatBowler = o.bowler.toString();
        if(thisBatsman.compareTo(thatBatsman) != 0) {
            return thisBatsman.compareTo(thatBatsman);
        }
        if(thisBowler.compareTo(thatBowler) != 0) {
            return thisBowler.compareTo(thatBowler);
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        BatBowl that = (BatBowl) o;
        return this.batsman.equals(that.batsman) &&
               this.bowler.equals(that.bowler);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.batsman, this.bowler);
    }
}