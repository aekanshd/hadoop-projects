 package IPL;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import IPL.BowlerData; 
import IPL.BatBowl;
import IPL.Constants;

public class IPLMapper extends Mapper<LongWritable, Text, BatBowl, BowlerData> {

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		String valueString = value.toString();
		String[] BatsmanData = valueString.split(",");
		
		if(BatsmanData[Constants.col_info].equalsIgnoreCase("ball"))
		{
			try {
				String striker = BatsmanData[Constants.col_batsman];
				String bowler = BatsmanData[Constants.col_bowler];
				String wicket = BatsmanData[Constants.col_wicket];
				String out_batsman = BatsmanData[Constants.col_batsman_out];
				
				BatBowl batBowl = new BatBowl();
				BowlerData bowlerData = new BowlerData();
				batBowl.setBowler(new Text(bowler));

				if(wicket.length() > 2) {
					batBowl.setBatsman(new Text(out_batsman));
					if(wicket.equalsIgnoreCase("run out") || wicket.equalsIgnoreCase("hit wicket") || wicket.equalsIgnoreCase("obstructing the field") || wicket.equalsIgnoreCase("retired hurt")) {
						batBowl.setBatsman(new Text(striker));
						bowlerData.setDelivery(Constants.one);
						bowlerData.setWicket(Constants.zero);		
					}
					else {
						batBowl.setBatsman(new Text(out_batsman));
						bowlerData.setDelivery(Constants.one);
						bowlerData.setWicket(Constants.one);
					}
				}
				else {
					batBowl.setBatsman(new Text(striker));					
					bowlerData.setDelivery(Constants.one);
					bowlerData.setWicket(Constants.zero);
				}

				context.write(batBowl, bowlerData);
			}				
			catch (NullPointerException e) {
				System.err.println("Key error.");
			}
			catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("OutOfBoundsError");
			}
		}
	}
}
