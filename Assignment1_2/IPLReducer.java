package IPL;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import IPL.BowlerData;
import IPL.BatBowl;

public class IPLReducer extends Reducer<BatBowl, BowlerData, BatBowl, BowlerData> {

	public void reduce(BatBowl t_key, Iterable<BowlerData> values, Context context) throws IOException, InterruptedException {
		BowlerData finalBowlerData = new BowlerData();

		int wickets = finalBowlerData.getWicket().get();
		int deliveries = finalBowlerData.getDelivery().get();
		
		for(BowlerData val : values) {
            wickets += val.getWicket().get();
            deliveries += val.getDelivery().get();
		}

        finalBowlerData.setWicket(new IntWritable(wickets));
        finalBowlerData.setDelivery(new IntWritable(deliveries));

		if(finalBowlerData.getDelivery().get() >= 5) context.write(t_key, finalBowlerData);
	}
}
