#!/bin/sh
clear
export CLASSPATH="$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.2.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-common-3.2.0.jar:$HADOOP_HOME/share/hadoop/common/hadoop-common-3.2.0.jar:~/Assignment1_2/*:$HADOOP_HOME/lib/*"
export CLASSPATH=$CLASSPATH:"~/Assignment1_2/"
rm -R IPL/ 
rm Assignment1_2.jar
$HADOOP_HOME/bin/hdfs dfs -rm /IPLTestDataTmp/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLTestDataTmp 
$HADOOP_HOME/bin/hdfs dfs -rm /IPLData_out_1_2/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLData_out_1_2/
javac -d . IPLDriver.java IPLMapper.java IPLFinalMapper.java IPLReducer.java Constants.java FinalData.java BowlerData.java BatBowl.java NaturalKeyPartitioner.java NaturalKeyGroupingComparator.java CompositeKeyComparator.java IPLFinalReducer.java
jar cfm Assignment1_2.jar Manifest.txt IPL/*.class
$HADOOP_HOME/bin/hadoop jar Assignment1_2.jar /IPLData /IPLData_out_1_2
$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out_1_2/part-r-00000 > output/assignment_task2.csv