package IPL;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class BowlerData implements WritableComparable<BowlerData> {
    private IntWritable wicket;
    private IntWritable delivery;

    public BowlerData() {
        wicket = new IntWritable(0);
        delivery = new IntWritable(0);
    }

    public void write(DataOutput dataOutput) throws IOException {
        wicket.write(dataOutput);
        delivery.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        wicket.readFields(dataInput);
        delivery.readFields(dataInput);
    }

    public IntWritable getWicket() {
        return wicket;
    }

    public void setWicket(IntWritable wicket) {
        this.wicket = wicket;
    }

    public IntWritable getDelivery() {
        return delivery;
    }

    public void setDelivery(IntWritable delivery) {
        this.delivery = delivery;
    }
    
    @Override
    public String toString() {
        return (wicket.toString()+","+delivery.toString());
    }

    public int compareTo(BowlerData o) {
        String thisWicket = this.wicket.toString();
        String thatWicket = o.wicket.toString();
        String thisDelivery = this.delivery.toString();
        String thatDelivery = o.delivery.toString();
        return thisWicket.compareTo(thatWicket) + thisDelivery.compareTo(thatDelivery);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BowlerData that = (BowlerData) o;
        return Objects.equals(delivery, that.delivery) &&
                Objects.equals(wicket, that.wicket);
    }
    @Override
    public int hashCode() {
        return Objects.hash(wicket, delivery);
    }
}