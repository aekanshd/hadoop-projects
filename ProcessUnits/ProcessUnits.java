package units; 

import java.util.*; 

import java.io.IOException; 
import java.io.DataInput; 
import java.io.DataOutput; 

import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.conf.*; 
import org.apache.hadoop.io.*; 
import org.apache.hadoop.mapred.*; 
import org.apache.hadoop.util.*; 

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class ProcessUnits implements WritableComparable<ProcessUnits> {
   
   private String symbol,
   private Long timstamp;

   public ProcessUnits() {}

   public ProcessUnits(Sting symbol, Long timestamp) {
      this.symbol = symbol;
      this.timestamp = timestamp;
   }

   @Override
   public String toString() {
      return(new StringBuilder())
         .append('{')
         .append(symbol)
         .append(',')
         .append('timestamp')
         .append('')
         .toString();
   }

   @Override
   public void readFields(DataInput in) throws IOException {
      symbol = WritableUtils.readString(in);
      timestamp = in.readLong();
   }

   @Override
   public void write(DataOutput out) throws IOException {
      WritableUtils.writeString(out, symbol);
      out.writeLong(timestamp);
   }

   @Override
   public int compareTo(StockKey o) {
      int result = symbol.compareTo(o.symbol);
      if(0 == result) {
         result = timestamp.compareTo(o.timestamp);
      }
      return result; // if symbols are same then return timestamp comparison or else symbols are diff
   }


   /**
    * Gets the symbol.
    * @return Symbol.
    */
   public String getSymbol() {
      return symbol;
   }

   public void setSymbol(String symbol) {
      this.symbol = symbol;
   }

   /**
    * Gets the timestamp.
    * @return Timestamp. i.e. the number of milliseconds since January 1, 1970, 00:00:00 GMT
    */
   public Long getTimestamp() {
      return timestamp;
   }

   public void setTimestamp(Long timestamp) {
      this.timestamp = timestamp;
   }





   //Mapper class 
   public static class E_EMapper extends MapReduceBase implements 
   Mapper<LongWritable ,/*Input key Type */ 
   Text,                /*Input value Type*/ 
   Text,                /*Output key Type*/ 
   IntWritable>        /*Output value Type*/ 
   {
      //Map function 
      public void map(LongWritable key, Text value, 
      OutputCollector<Text, IntWritable> output,   
      
      Reporter reporter) throws IOException { 
         String line = value.toString();  // Comma separated StockSymbol, Timstamp, Price
         String lasttoken = null; 
         StringTokenizer s = new StringTokenizer(line,","); 
         String stockSymbol = s.nextToken();

         System.out.println("Line: "+ line.toString());
         System.out.println("stockSymbol: "+ stockSymbol.toString());

         while(s.hasMoreTokens()) {
            lasttoken = s.nextToken();
            System.out.println("Inside While: "+lasttoken.toString());
         }
            
            System.out.println("Last token: " + lasttoken);               
            
         int avgprice = Integer.parseInt(lasttoken); 
         output.collect(new Text(year), new IntWritable(avgprice)); 
      } 
   }
   
   //Reducer class 
   public static class E_EReduce extends MapReduceBase implements Reducer< Text, IntWritable, Text, IntWritable > {
   
      //Reduce function 
      public void reduce( Text key, Iterator <IntWritable> values, 
      OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException { 
         int maxavg = 30; 
         int val = Integer.MIN_VALUE; 
            
         while (values.hasNext()) { 
            if((val = values.next().get())>maxavg) { 
               output.collect(key, new IntWritable(val)); 
            } 
         }
      } 
   
   }

   //Main function 
   public static void main(String args[])throws Exception { 
      JobConf conf = new JobConf(ProcessUnits.class); 
      
      conf.setJobName("max_eletricityunits"); 
      conf.setOutputKeyClass(Text.class);
      conf.setOutputValueClass(IntWritable.class); 
      conf.setMapperClass(E_EMapper.class); 
      conf.setCombinerClass(E_EReduce.class); 
      conf.setReducerClass(E_EReduce.class); 
      conf.setInputFormat(TextInputFormat.class); 
      conf.setOutputFormat(TextOutputFormat.class); 
      
      FileInputFormat.setInputPaths(conf, new Path(args[0])); 
      FileOutputFormat.setOutputPath(conf, new Path(args[1])); 
      
      JobClient.runJob(conf); 
   } 
}
