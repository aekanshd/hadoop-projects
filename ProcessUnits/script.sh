#!/bin/sh
export CLASSPATH="/usr/local/hadoop/share/hadoop/common/hadoop-common-2.7.2.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.7.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar"
export CLASSPATH=$CLASSPATH:"~/Desktop/hadoop-projects/ProcessUnits/"
rm -R units/ && rm ProcessUnits.jar
$HADOOP_HOME/bin/hdfs dfs -rm /ProcessUnits/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /ProcessUnits/
javac -d . ProcessUnits.java 
jar cfm ProcessUnits.jar Manifest.txt units/*.class
$HADOOP_HOME/bin/hadoop jar ProcessUnits.jar /ProcessUnitsData /ProcessUnits
$HADOOP_HOME/bin/hdfs dfs -cat /ProcessUnits/part-00000