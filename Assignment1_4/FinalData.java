package IPL;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class FinalData implements WritableComparable<FinalData> {
    private Text batsman;
    private Text stadium;
    private IntWritable runs;
    private IntWritable deliveries;
    private FloatWritable strikeRate;

    public FinalData() {
        batsman = new Text();
        stadium = new Text();
        runs = new IntWritable(0);
        deliveries = new IntWritable(0);
        strikeRate = new FloatWritable(0);
    }

    public void write(DataOutput dataOutput) throws IOException {
        batsman.write(dataOutput);
        stadium.write(dataOutput);
        runs.write(dataOutput);
        deliveries.write(dataOutput);
        strikeRate.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        batsman.readFields(dataInput);
        stadium.readFields(dataInput);
        runs.readFields(dataInput);
        deliveries.readFields(dataInput);
        strikeRate.readFields(dataInput);
    }

    public Text getBatsman() {
        return batsman;
    }

    public void setBatsman(Text batsman) {
        this.batsman = batsman;
    }

    public Text getStadium() {
        return stadium;
    }

    public void setStadium(Text stadium) {
        this.stadium = stadium;
    }

    public IntWritable getRuns() {
        return runs;
    }

    public void setRuns(IntWritable runs) {
        this.runs = runs;
    }

    public IntWritable getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(IntWritable deliveries) {
        this.deliveries = deliveries;
    }

    public FloatWritable getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(FloatWritable strikeRate) {
        this.strikeRate = strikeRate;
    }


    @Override
    public String toString() {
        return (stadium + "," + batsman + "," + runs.toString() + "," + deliveries.toString() + "," + strikeRate.toString());
        // return (runs.toString() + "," + deliveries.toString() + "," + batsman + "," + stadium);
    }

    @Override
    public int compareTo(FinalData o) {
        System.out.println("Inside compareTo of FinalData.");
        
        Text bm2 = o.batsman;
        Text v2 = o.stadium;       // bo2
        Text bm1 = this.batsman;
        Text v1 = this.stadium;    // bo1
        IntWritable w2 = o.runs;
        IntWritable d2 = o.deliveries;
        FloatWritable s2 = o.strikeRate;
        IntWritable w1 = this.runs;
        IntWritable d1 = this.deliveries;
        FloatWritable s1 = this.strikeRate;

        int wi1, wi2, di1, di2;
        float sr1, sr2;
        wi1 = w1.get();
        wi2 = w2.get();
        di1 = d1.get();
        di2 = d2.get();
        sr1 = d2.get();
        sr2 = d2.get();

        if(v1.compareTo(v2) != 0) {
            return v1.compareTo(v2);
        }
        if(sr1 != sr2) {
            return -((sr1 - sr2) > 0 ? 1 : -1);
        }
        if(bm1.compareTo(bm2) != 0) {
            return bm1.compareTo(bm2);
        }
        // if(wi1 != wi2) {
        //     return -(wi1 - wi2);
        // }
        // if(di1 != di2) {
        //     return (di1 - di2);
        // }
                
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinalData that = (FinalData) o;
        return Objects.equals(batsman, that.batsman) &&
                Objects.equals(stadium, that.stadium) &&
                Objects.equals(strikeRate, that.strikeRate);
                // Objects.equals(runs, that.runs) &&
                // Objects.equals(deliveries, that.deliveries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.stadium, this.strikeRate, this.batsman);
        // return Objects.hash(this.stadium, this.batsman, this.runs, this.deliveries);
    }
}