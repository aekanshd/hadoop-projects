package IPL;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.chain.ChainMapper;
import org.apache.hadoop.mapreduce.lib.chain.ChainReducer;

public class IPLDriver extends Configured implements Tool {
	
	@Override
    public int run(String[] args) throws Exception {
    	Path outTmp = new Path("/IPLTestDataTmp");
    	
    	// Create First Job
	    Configuration conf = new Configuration();
		conf.set("mapred.textoutputformat.separator", ",");
        
        Job job1 = Job.getInstance(conf, "IPLv3");
        job1.setJarByClass(getClass());
				
		// Create the chain
		job1.setMapperClass(IPL.IPLMapper.class);
		job1.setReducerClass(IPL.IPLReducer.class);

		// Set input output class
		job1.setOutputKeyClass(BatBowl.class);
		job1.setOutputValueClass(BowlerData.class);

		// Set input output paths
		FileInputFormat.setInputPaths(job1, new Path(args[0]));
		FileOutputFormat.setOutputPath(job1, outTmp);

		if(!job1.waitForCompletion(true)) {
			System.exit(1);
		}

		// Creaet post reducer job
        Job job2 = Job.getInstance(conf, "IPLv3FinalData");
        job2.setJarByClass(getClass());
				
		// Create the chain
		job2.setMapperClass(IPL.IPLFinalMapper.class);
		job2.setReducerClass(IPL.IPLFinalReducer.class);

		job2.setPartitionerClass(IPL.NaturalKeyPartitioner.class);
		job2.setGroupingComparatorClass(IPL.NaturalKeyGroupingComparator.class);
		job2.setSortComparatorClass(IPL.CompositeKeyComparator.class);

		// Set input output class
		job2.setOutputKeyClass(IPL.FinalData.class);
		job2.setOutputValueClass(NullWritable.class);

		// Set input output paths
		FileInputFormat.setInputPaths(job2, outTmp);
		FileOutputFormat.setOutputPath(job2, new Path(args[1]));

		return job2.waitForCompletion(true)? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
        int exitFlag = ToolRunner.run(new IPLDriver(), args);
        System.exit(exitFlag);
    }
}
