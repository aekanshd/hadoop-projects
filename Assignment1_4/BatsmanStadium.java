package IPL;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class BatsmanStadium implements WritableComparable<BatsmanStadium> {
    private Text batsman;
    private Text stadium;

    public BatsmanStadium() {
        batsman = new Text();
        stadium = new Text();
    }

    public void write(DataOutput dataOutput) throws IOException {
        batsman.write(dataOutput);
        stadium.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        batsman.readFields(dataInput);
        stadium.readFields(dataInput);
    }

    public Text getBatsman() {
        return batsman;
    }

    public void setBatsman(Text batsman) {
        this.batsman = batsman;
    }

    public Text getStadium() {
        return stadium;
    }

    public void setStadium(Text stadium) {
        this.stadium = stadium;
    }


    @Override
    public String toString() {
        return (stadium + "," + batsman);
    }

    @Override
    public int compareTo(BatsmanStadium o) {
        
        Text bm2 = o.batsman;
        Text v2 = o.stadium;       // bo2
        Text bm1 = this.batsman;
        Text v1 = this.stadium;   

        if(v1.compareTo(v2) != 0) {
            return v1.compareTo(v2);
        }
        if(bm1.compareTo(bm2) != 0) {
            return bm1.compareTo(bm2);
        }

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BatsmanStadium that = (BatsmanStadium) o;
        return Objects.equals(batsman, that.batsman) &&
                Objects.equals(stadium, that.stadium);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.stadium, this.batsman);
    }
}