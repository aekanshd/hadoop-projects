 package IPL;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import IPL.BowlerData; 
import IPL.BatBowl;
import IPL.Constants;

public class IPLMapper extends Mapper<LongWritable, Text, BatBowl, BowlerData> {

	String stadium = "";

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		String valueString = value.toString();
		String[] BatsmanData = valueString.split(",");
		
		if(BatsmanData[Constants.col_info].equalsIgnoreCase("info")) {
			if (BatsmanData[Constants.col_venue].equalsIgnoreCase("venue")) {
				try {
					stadium = BatsmanData[Constants.col_stadium].split(", ")[0].replace("\"", "");
					// System.err.println("stadium found... " + stadium);
				}				
				catch (NullPointerException e) {
					System.err.println("Key error... venue");
				}
				catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("OutOfBoundsError");
				}
			}
		} else {
			if(BatsmanData[Constants.col_info].equalsIgnoreCase("ball"))
			{
				try {
					String batsman = BatsmanData[Constants.col_batsman];
					// System.out.println("batsman: "+batsman);

					String runs = BatsmanData[Constants.col_runs];
					// System.out.println("runs:" + runs);

					
					BatBowl batBowl = new BatBowl();
					BowlerData bowlerData = new BowlerData();

					// System.out.println("stadium: "+stadium);
					// Set stadium, batsman, runs, and balls played
					batBowl.setStadium(new Text(stadium));
					batBowl.setBatsman(new Text(batsman));
					bowlerData.setDelivery(Constants.one);
					bowlerData.setRuns(new IntWritable(Integer.parseInt(runs)));

					// System.out.println("batBowl: " + batBowl.toString());
					// System.out.println("bowlerData: " + bowlerData.toString());
					
					try {
						context.write(batBowl, bowlerData);
					}
					catch (NullPointerException e) {
						System.err.println("Context error: " + e);
					}
				}				
				catch (NullPointerException e) {
					System.err.println("Other error: " + e);
				}
				catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("OutOfBoundsError");
				}
			} // if ball
		} // if info
	}
}
