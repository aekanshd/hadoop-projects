package IPL;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import IPL.BowlerData;
import IPL.BatBowl;

public class IPLReducer extends Reducer<BatBowl, BowlerData, BatBowl, BowlerData> {

	public void reduce(BatBowl t_key, Iterable<BowlerData> values, Context context) throws IOException, InterruptedException {
		BowlerData finalBowlerData = new BowlerData();

		int runs = finalBowlerData.getRuns().get();
		int deliveries = finalBowlerData.getDelivery().get();
		int number = 0;
		
		for(BowlerData val : values) {
			number += 1;
            runs += val.getRuns().get();
            deliveries += val.getDelivery().get();
		}

		float strikeRate = (runs * 100) / deliveries;
		// System.out.println("Reducer.StrikeRate: " + strikeRate);

        finalBowlerData.setRuns(new IntWritable(runs));
        finalBowlerData.setDelivery(new IntWritable(deliveries));
        finalBowlerData.setStrikeRate(new FloatWritable(strikeRate));

        if(number >= 10) context.write(t_key, finalBowlerData);
	}
}
