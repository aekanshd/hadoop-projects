package IPL;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import IPL.BowlerData;
import IPL.BatBowl;
import IPL.Constants;
import IPL.OverallWrapper;

public class IPLMapper extends MapReduceBase implements Mapper<LongWritable, Text, BatBowl, BowlerData> {

	public void map(OverallWrapper key, NullWritable value, OutputCollector<BatBowl, BowlerData> output, Reporter reporter) throws IOException {
		output.collect(key.batBowl, key.bowlerData);
	}
}
