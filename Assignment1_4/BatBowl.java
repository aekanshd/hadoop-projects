package IPL;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import java.util.ArrayList;
import java.util.List;

public class BatBowl implements WritableComparable<BatBowl> {
    private Text batsman;
    private Text stadium;

    public BatBowl() {
        batsman = new Text();
        stadium = new Text();
    }

    public void write(DataOutput dataOutput) throws IOException {
        batsman.write(dataOutput);
        stadium.write(dataOutput);
    }
    
    public void readFields(DataInput dataInput) throws IOException {
        batsman.readFields(dataInput);
        stadium.readFields(dataInput);
    }

    public Text getBatsman() {
        return batsman;
    }

    public void setBatsman(Text batsman) {
        this.batsman = batsman;
    }

    public Text getStadium() {
        return stadium;
    }

    public void setStadium(Text stadium) {
        this.stadium = stadium;
    }

    @Override
    public String toString() {
        return (stadium + "," + batsman);
    }

    public int compareTo(BatBowl o) {
        String thisBatsman = this.batsman.toString();
        String thatBatsman = o.batsman.toString();
        String thisStadium = this.stadium.toString();
        String thatStadium = o.stadium.toString();
        
        if(thisStadium.compareTo(thatStadium) != 0) {
            return thisStadium.compareTo(thatStadium);
        }
        if(thisBatsman.compareTo(thatBatsman) != 0) {
            return thisBatsman.compareTo(thatBatsman);
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        BatBowl that = (BatBowl) o;
        return this.batsman.equals(that.batsman) &&
               this.stadium.equals(that.stadium);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.stadium, this.batsman);
    }
}