package IPL;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import IPL.BowlerData;
 
public class BowlerDataComparator extends WritableComparator {
	
	protected BowlerDataComparator() {
		super(BowlerData.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
	 
		BowlerData key1 = (BowlerData) w1;
		BowlerData key2 = (BowlerData) w2;

		if (key1.getWicket().get() > key2.getWicket().get()) {
			return 1;
		}
		else if(key1.getWicket().get() < key2.getWicket().get()) {
			return -1;
		}
		else {
			if(key1.getDelivery().get() < key2.getDelivery().get()) {
				return 1;
			}
			else if(key1.getDelivery().get() > key2.getDelivery().get()) {
				return -1;
			}
			else {
				return 0;
			}
		}
	}
}