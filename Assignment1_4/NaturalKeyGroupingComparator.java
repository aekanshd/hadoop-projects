package IPL;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;

public class NaturalKeyGroupingComparator extends WritableComparator {

	protected NaturalKeyGroupingComparator() {
		super(IPL.FinalData.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable wc1, WritableComparable wc2) {
		FinalData k1 = (FinalData)wc1;
		FinalData k2 = (FinalData)wc2;
		
		// Extra code written for full sorting

        Text bm2 = k2.getBatsman();
        Text v2 = k2.getStadium();      // bo2
        Text bm1 = k1.getBatsman();
        Text v1 = k1.getStadium();      // bo1
        IntWritable w2 = k2.getRuns();
        IntWritable d2 = k2.getDeliveries();
        IntWritable w1 = k1.getRuns();
        IntWritable d1 = k1.getDeliveries();
        FloatWritable s1 = k1.getStrikeRate();
        FloatWritable s2 = k2.getStrikeRate();

        int wi1, wi2, di1, di2;
        float sr1, sr2;

        wi1 = w1.get();
        wi2 = w2.get();
        di1 = d1.get();
        di2 = d2.get();
        sr1 = s1.get();
        sr2 = s2.get();

        if(sr1 != sr2) {
            return (sr1 > sr2 ? 1 : -1);
        }
        if(v1.compareTo(v2) != 0) {
            return v1.compareTo(v2);
        }
        if(bm1.compareTo(bm2) != 0) {
            return bm1.compareTo(bm2);
        }
   
        return 0;
	}
}
