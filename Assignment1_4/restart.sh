#!/bin/sh
clear
export CLASSPATH="$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.2.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-common-3.2.0.jar:$HADOOP_HOME/share/hadoop/common/hadoop-common-3.2.0.jar:~/Assignment1_2/*:$HADOOP_HOME/lib/*"
export CLASSPATH=$CLASSPATH:"~/Assignment1_4/"
rm -R IPL/ 
rm Assignment1_4.jar
$HADOOP_HOME/bin/hdfs dfs -rm -r /IPLTestDataTmp/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLTestDataTmp 
$HADOOP_HOME/bin/hdfs dfs -rm /IPLData_out_1_4/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /IPLData_out_1_4/

javac -d . IPLDriver.java IPLMapper.java IPLFinalMapper.java IPLReducer.java Constants.java FinalData.java BatsmanStadium.java BowlerData.java BatBowl.java NaturalKeyPartitioner.java NaturalKeyGroupingComparator.java CompositeKeyComparator.java IPLFinalReducer.java
jar cfm Assignment1_4.jar Manifest.txt IPL/*.class

$HADOOP_HOME/bin/hadoop jar Assignment1_4.jar /IPLData /IPLData_out_1_4
$HADOOP_HOME/bin/hdfs dfs -cat /IPLData_out_1_4/part-r-00000 > output/assignment_task4.csv