package IPL;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.IOException;

public class CompositeKeyComparator extends WritableComparator {

	protected CompositeKeyComparator() {
		super(IPL.FinalData.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		// This compare is finally implemented
		FinalData k1 = (FinalData)w1;
		FinalData k2 = (FinalData)w2;
		
		int result = k1.getStadium().compareTo(k2.getStadium());

		if(0 == result) {
			result = -1 * k1.getStrikeRate().compareTo(k2.getStrikeRate());
			if(0 == result) {
				result = k1.getBatsman().compareTo(k2.getBatsman());
			}
		}

		return result;
	}
}
