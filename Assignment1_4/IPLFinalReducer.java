package IPL;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IPLFinalReducer extends Reducer<FinalData, NullWritable, BatsmanStadium, NullWritable> {

	Text bestBatsman = null;
	float bestStrikeRate = 0.0f;
	Text currentStadium = null;
	String currSt = null, nextSt = "";

	public void reduce(FinalData t_key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {		
        
		BatsmanStadium bs = new BatsmanStadium();
		bs.setStadium(t_key.getStadium());
		bs.setBatsman(t_key.getBatsman());

		nextSt = bs.getStadium().toString();

		if(currSt == null) {
			currSt = bs.getStadium().toString();
        	bestBatsman = bs.getBatsman();
        	context.write(bs, NullWritable.get());
		} else if(currSt.compareTo(nextSt) != 0) {
			currSt = bs.getStadium().toString();
        	bestBatsman = bs.getBatsman();
        	context.write(bs, NullWritable.get());
		} 
	}
}
