# Hadoop (Big Data) Projects

## List of Projects

- Basic WordCount tutorial.
- Find number of gas cars in each city.
- Find out Number of Products Sold in Each Country.

## Commands

1. Export PATH

    ```shell
    export CLASSPATH="$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.2.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-common-3.2.0.jar:$HADOOP_HOME/share/hadoop/common/hadoop-common-3.2.0.jar:~/PackageName/*:$HADOOP_HOME/lib/*"
    ```

1. Make Package

    ```shell
    javac -d . [all classes.java separated by space]
    ```

2. Make JAR

    ```shell
    jar cfm [MyJARName].jar Manifest.txt [PackageName]/*.class
    ```

3. Use JAR on Dataset

   ```shell
   $HADOOP_HOME/bin/hadoop jar [MyJARName].jar /relative/path/to/dataset/from/hdfs/home /out/dir/path
   ```