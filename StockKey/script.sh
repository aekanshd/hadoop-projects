#!/bin/sh
clear
export CLASSPATH="/usr/local/hadoop/share/hadoop/common/hadoop-common-2.7.2.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.7.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar"
export CLASSPATH=$CLASSPATH:"~/Desktop/hadoop-projects/StockKey/"
export CLASSPATH=$CLASSPATH:"~/Desktop/hadoop-projects/StockKey/org-apache-commons-logging.jar"
rm -R demo/ && rm StockKey.jar
$HADOOP_HOME/bin/hdfs dfs -rm /StockKey/* 
$HADOOP_HOME/bin/hdfs dfs -rmdir /StockKey/
javac -d . CompositeKeyComparator.java NaturalKeyGroupingComparator.java NaturalKeyPartitioner.java SsJob.java SsMapper.java SsReducer.java StockKey.java
jar cfm StockKey.jar Manifest.txt demo/*.class
$HADOOP_HOME/bin/hadoop jar StockKey.jar /StockKeyData /StockKey
$HADOOP_HOME/bin/hdfs dfs -cat /StockKey/part-r-00000