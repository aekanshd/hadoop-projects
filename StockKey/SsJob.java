/**
 * Copyright 2012 Jee Vang 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0 
 *  
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License. 
 */
package demo;

import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.io.*; 
import org.apache.hadoop.conf.*; 
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.mapreduce.*; 
import org.apache.hadoop.util.*; 

import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.chain.ChainMapper;
import org.apache.hadoop.mapreduce.lib.chain.ChainReducer;

/**
 * Secondary sort job.
 * @author Jee Vang
 *
 */
public class SsJob extends Configured implements Tool {
	/**
	 * Main method. You should specify -Dmapred.input.dir and -Dmapred.output.dir.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

        // int exitFlag = ToolRunner.run(new IPLDriver(), args);
        // System.exit(exitFlag);
		ToolRunner.run(new Configuration(), new SsJob(), args);
	}
	
	@Override
	public int run(String[] args) throws Exception {

		// Create job
		Configuration conf = getConf();
		Job job = new Job(conf, "secondary sort");		
		job.setJarByClass(SsJob.class);
		
		// File input and output paths
      	FileInputFormat.setInputPaths(job, new Path(args[0])); 
      	FileOutputFormat.setOutputPath(job, new Path(args[1])); 

		// Set Mapper
		job.setMapperClass(SsMapper.class);
		job.setMapOutputKeyClass(StockKey.class);
		job.setMapOutputValueClass(DoubleWritable.class);
		
		// Set partitioner statement
		job.setPartitionerClass(NaturalKeyPartitioner.class);
		job.setGroupingComparatorClass(NaturalKeyGroupingComparator.class);
		job.setSortComparatorClass(CompositeKeyComparator.class);

		// Set reducer class
		job.setReducerClass(SsReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		// Input output format for data
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);		
		
		// Run the job
		job.waitForCompletion(true);

		return 0;
	}

}
